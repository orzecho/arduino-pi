from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np

# cap = cv2.VideoCapture(0)
img = cv2.imread('red.png')
TRESHOLD = 2

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))
time.sleep(0.1)

def isRed(res):
    splited = cv2.split(res)
    mean = np.mean([np.mean(splited[0]), np.mean(splited[1]), np.mean(splited[2])])
    #print(mean)
    return mean > TRESHOLD

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):

    # Take each frame
    # _, frame = cap.read()
    frame = frame.array

    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    lower_red = np.array([170, 100, 100])
    upper_red = np.array([200, 255, 255])

    # Threshold the HSV image to get only blue colors
    mask = cv2.inRange(hsv, lower_red, upper_red)

    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask= mask)

    cv2.imshow('frame',frame)
    cv2.imshow('mask',mask)
    cv2.imshow('res',res)

    print(isRed(res))
    rawCapture.truncate(0)