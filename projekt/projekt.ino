#include <Servo.h>
#include <SharpIR.h>

Servo myservo;
SharpIR SharpIR(A0, 20150);

void setup() {
  pinMode(7, OUTPUT); 
  pinMode(8, OUTPUT); 
  pinMode(12, OUTPUT); 
  pinMode(13, OUTPUT); 
  pinMode(10, OUTPUT);  //PWM
  pinMode(11, OUTPUT);  //PWM
  
  myservo.attach(A1);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4,OUTPUT);
  pinMode(A5, INPUT);
  Serial.begin(9600);

//  Serial.begin(38400);
//  Serial1.begin(38400);
//  Serial1.println("Start");
}

void setPins(int a7, int a8, int a12, int a13, int a10, int a11) {
  digitalWrite(7, a7);// 12 i 13 do 11 pina
  digitalWrite(8, a8);
  digitalWrite(12, a12);
  digitalWrite(13, a13);
  analogWrite(10, a10);
  analogWrite(11, a11);
}
void stop() {
  digitalWrite(7, LOW);
  digitalWrite(8, LOW);
  digitalWrite(12, LOW);
  digitalWrite(13, LOW);
  digitalWrite(10, LOW);
  digitalWrite(11, LOW);
}

void move(int distance)
{
  int counter = 0;
  int prev;
  int act;
  
  if (distance > 0) {
    setPins(LOW, HIGH, LOW, HIGH, 255, 255); 
  } else {
    setPins(HIGH, LOW, HIGH, LOW, 255, 255);  
    distance = distance * (-1);
  }
  
  while(counter < distance)
  {
    act = digitalRead(A2);
    if(act != prev  && act == 1)
    {
      counter++;
    }
    prev = act;
  }
  
  stop();

}

void rotate(int angle){
  angle = angle / 6;
  int counter = 0;
  int prev;
  int act;

  if (angle > 0) {
    setPins(HIGH, LOW, LOW, HIGH, 255, 255); 
  } else {
    setPins(LOW, HIGH, HIGH, LOW, 255, 255);  
    angle = -angle;
  }

  while(counter < angle)
  {
    act  = digitalRead(A2);
    if(act != prev  && act == 1)
    {
      counter++;
    }
    prev = act;
  }

  stop();
}

int getBackDistance() {
  digitalWrite(A4, LOW);
  delayMicroseconds(2);
  digitalWrite(A4, HIGH);
  delayMicroseconds(10);
  digitalWrite(A4, LOW);
  long duration = pulseIn(A5, HIGH);
  return duration*0.034/2;  
}

int getFrontDistance() {
  int distance = SharpIR.distance();
  if(distance > 150) {
    distance = 150;  
  }
  return distance;
}

void scan() {
  int angle = 0;
  myservo.write(angle);
  delay(500);
  while(angle <= 180) {
    int distance = scanStep(angle);
    angle += 10;
    Serial.println(distance);
  }
  while(angle >= 0) {
    int distance = scanStep(angle);
    angle -= 10;
    Serial.println(distance);
  }
}

int scanStep(int angle) {
  myservo.write(angle);
  delay(100);
  int distance = getFrontDistance();
  return distance;
}

int fastScanStep(int angle) {
  myservo.write(angle);
  int distance = getFrontDistance();
  return distance;
}

void goUntilWall() {
  int angle = 90;
  myservo.write(angle);
  delay(50);
  int distance = getFrontDistance();

  while(distance > 15) {
    move(-10);
    while(angle <= 150) {
      distance = fastScanStep(angle);
      angle += 10;
    }
    while(angle >= 30) {
      distance = fastScanStep(angle);
      angle -= 10;
    }
  }
  
}

void readFromSerial() {
  if (Serial.available() > 0) {
                String command = Serial.readString();
                if(command[0] == 'b') {
                  int value = command.substring(2).toInt();
                  move(-value);
                }
                else if(command[0] == 'm') {
                  int value = command.substring(2).toInt();
                  move(value);
                }
                else if(command[0] == 'r') {
                  int value = command.substring(2).toInt();
                  rotate(value);
                }
                else if(command[0] == 'l') {
                  int value = command.substring(2).toInt();
                  rotate(-value);
                }
                else if(command[0] == 's') {
                  scan();
                }
                else if(command[0] == 'x') {
                  Serial.println(getBackDistance());
                }
                else if(command[0] == 'c') {
                  Serial.println(getFrontDistance());
                }
                else if(command[0] == 'p') {
                  int value = command.substring(2).toInt();
                  myservo.write(value);
                }
                else if(command[0] == 'w') {
                  int value = command.substring(2).toInt();
                  myservo.write(value);
                  Serial.println(getFrontDistance());
                }
                else if(command[0] == 'z') {
                  goUntilWall();
                }
                else if(command[0] == 'y') {
                  goUntilWall();
                  rotate(90);
                  goUntilWall();
                  rotate(90);
                  goUntilWall();
                  rotate(90);
                  goUntilWall();
                  rotate(90);
                }
  }  
}

void loop() {
  readFromSerial();
//  if(Serial1.available()>0)
//  {
//    Serial.write(Serial1.read());
//  }
//  
//  if(Serial.available()>0)
//  {
//    Serial1.write(Serial.read());
//  }
}

